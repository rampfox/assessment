let expression = (input) => {
    let bracket = "[]{}()<>"
    let stack = []

    for (let brackets of input) {
        let bracketsIndex = bracket.indexOf(brackets)
console.log(brackets)
console.log(bracketsIndex)
        if (bracketsIndex === -1){
            continue
        }

        if (bracketsIndex % 2 === 0) {
            stack.push(bracketsIndex + 1)
        } else {
            if (stack.pop() !== bracketsIndex) {
                return false;
            }
        }
    }
    console.log(stack)
    return stack.length === 0
}
console.log(expression(`({[]})`))